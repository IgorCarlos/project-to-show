<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\api;
use App\indicador;
use App\indicado;
use Mockery\Exception;
use PHPMailerAutoload;
use PHPMailer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class appController extends Controller
{
    public function addIndicados($email, Request $req)
    {
        
        $indicador = indicador::orderBy('id', 'DESC')->where('email', $email)->get();

        $qtd = sizeof($indicador);

        $userData = null;
        if (!$qtd) {

            $url = "http://api.vtexcrm.com.br/semparar/dataentities/CL/search?_fields=firstName,email,phone&_where=email=" . $email;

            $response = httpGet($url);

            $dadosIndicador = json_decode($response);
            
            
            if(sizeOf($dadosIndicador) != 0){    
                $inputIndicador = Array(
                    'nome' => $dadosIndicador[0]->firstName,
                    'email' => $dadosIndicador[0]->email
                );

                $inputTrigger = Array(
                    'nomeindicador' => $dadosIndicador[0]->firstName,
                    'emailindicador' => $dadosIndicador[0]->email
                );

                $inputTrigger['nomeindicador'] = $dadosIndicador[0]->firstName;
                $inputTrigger['emailindicador'] = $dadosIndicador[0]->email;
            }else{
                $inputIndicador = Array(
                    'nome' => $req->input('nomeIndicador'),
                    'email' => $req->input('emailIndicador')
                );
                
                $inputTrigger['nomeindicador'] = $req->input('nomeIndicador');
                $inputTrigger['emailindicador'] = $req->input('emailIndicador');

                
                /*= Array(
                    'nomeindicador' => $req->input('nomeIndicador'),
                    'emailindicador' => $req->input('emailIndicador')
                );*/

                $dadosVtex['firstName'] = $req->input('nomeIndicador');
                $dadosVtex['email'] = $req->input('emailIndicador');
                $dadosVtex['document'] = $req->input('cpfIndicador');
                $dadosVtex['documentType'] = 'cpf';

                $url = "http://api.vtexcrm.com.br/semparar/dataentities/CL/documents";

                $indicador = json_encode($dadosVtex);

                $respCad = httpPost($url, $indicador);
            }

            
            $urlTrigger = "http://api.vtexcrm.com.br/semparar/dataentities/GA/documents";


            $indicadorTrigger = json_encode($inputTrigger);
            
            $respCadTrigger = httpPost($urlTrigger, $indicadorTrigger);

            $getData = indicador::create($inputIndicador);
            $userData = json_decode($getData->where('id', $getData->id)->get());

        } else {
            $userData = json_decode($indicador);
        }

        
        $indicados = $req->input();


        foreach ($indicados['member'] as $key => $value) {

            if($value['email'] != ""){
                $verificaIndicado = json_decode(indicado::orderBy('id', 'desc')->where('email', $value['email'])->get());


                if (sizeof($verificaIndicado) == 0) {
                    $url = "http://api.vtexcrm.com.br/".api::$loja."/dataentities/".api::$md."/documents";

                    $value['idIndicador'] = $userData[0]->id;
                    $value['compraRealizada'] = 0;
                    $value['ligacaoEfetuada'] = 0;
                    $value['emailIndicador'] = $email;

                    indicado::create($value);

                    $obj = json_encode($value);

                    
                    $response = httpPost($url, $obj);

                }
            }

        }


        

        return 1;
    }

    public function addBuyToMember(Request $req){

        $dados = $req->input();
        $indicado = indicado::orderBy('id','desc')->where('email', $dados['emailMembro']);

        $input['idCompra'] = $dados['idCompra'];
        $input['compraRealizada'] = 1;
        $input['statusDescription'] = "Pedido não encontrado";


        $indicado->update($input);


    }

    public function getPagination(){
    }

    public function getAllMembers($id = ""){

        if($id != null){
            $indicados = indicado::orderBy('indicado.id','DESC')
                ->where('idIndicador', $id)
                ->join('indicador', 'indicado.idIndicador', '=', 'indicador.id')
                ->select('indicador.email as indicadorMail', 'indicador.nome as firstName', 'indicado.*')
                ->paginate(10);

        }else{
            $indicados = indicado::orderBy('indicado.id','DESC')
                ->join('indicador', 'indicado.idIndicador', '=', 'indicador.id')
                ->select('indicador.email as indicadorMail', 'indicador.nome as firstName', 'indicado.*')
                ->paginate(10);

        }

        return $indicados;
    }


    public function returnId($email){

        try{

            $url = "http://api.vtexcrm.com.br/semparar/dataentities/CL/search?_fields=id&_where=email=" . $email;

            $response = httpGet($url);

            $json = json_decode($response);

            return $json[0]->id;

        }catch(\Exception $exception){
            return 'erro';
        }

    }

    public function getQtdIndicadores(){
        $indicadores = indicador::orderBy('id','DESC');

        return sizeOf($indicadores);


    }

    public function filterMemberBy($filter){
        switch ($filter){
            case 'ligacaoASC':
                //$filter = "ligacaoEfetuada%20asc";
                $indicados = indicado::orderBy('ligacaoEfetuada', 'ASC')
                    ->join('indicador', 'indicado.idIndicador', '=', 'indicador.id')
                    ->select('indicador.email as indicadorMail', 'indicador.nome as firstName', 'indicado.*')
                    ->paginate(10);
                break;
            case 'ligacaoDESC':
                $indicados = indicado::orderBy('ligacaoEfetuada', 'DESC')
                    ->join('indicador', 'indicado.idIndicador', '=', 'indicador.id')
                    ->select('indicador.email as indicadorMail', 'indicador.nome as firstName', 'indicado.*')
                    ->paginate(10);
                break;
            case 'compraefetuadaASC':
                $indicados = indicado::orderBy('compraRealizada', 'ASC')
                    ->join('indicador', 'indicado.idIndicador', '=', 'indicador.id')
                    ->select('indicador.email as indicadorMail', 'indicador.nome as firstName', 'indicado.*')
                    ->paginate(10);
                break;
            case 'compraefetuadaDESC':
                $indicados = indicado::orderBy('compraRealizada', 'DESC')
                    ->join('indicador', 'indicado.idIndicador', '=', 'indicador.id')
                    ->select('indicador.email as indicadorMail', 'indicador.nome as firstName', 'indicado.*')
                    ->paginate(10);
                break;
            default:
                $filter = "";
                break;
        }


        return $indicados;
    }

    public function addCalledMember(Request $req){
        $indicado = indicado::findOrFail($req->input()['id']);

        $indicado->update($req->input());

    }

    public function addbuyMember(Request $req, $id){
        $indicado = indicado::findOrFail($id);


        $indicado['idCompra'] = $req->input()['idCompra'];
        $indicado['statusDescription'] = "Pagamento pendente";


        $indicado->update($req->input());

  //      echo "a";

    }

    public function filterMembers(Request $req){

        if($req->input()['search'] != ""){
            $data = indicado::orderBy('id', 'DESC')->orWhere('nome', 'like', '%' . $req->input()['search']. '%')->orWhere('email', 'like', '%' . $req->input()['search']. '%')->get();
        }else{
            $data = indicado::orderBy('indicado.id','DESC')
                ->join('indicador', 'indicado.idIndicador', '=', 'indicador.id')
                ->select('indicador.email as indicadorMail', 'indicador.nome as firstName', 'indicado.*')
                ->skip(0)
                ->take(10)
                ->get();
        }

        return $data;
    }


    public function filterIndicadores(Request $req){


        if($req->input()['search'] != ""){
            $indicados = indicador::orderBy('id','DESC')->orWhere('nome', 'like', '%' . $req->input()['search']. '%')->orWhere('email', 'like', '%' . $req->input()['search']. '%')
                ->get();

            $indicadores = array();
            foreach ($indicados as $key => $value){
                $qtd = DB::table('indicado')
                    ->where('idIndicador', $value['id'])
                    ->select(DB::raw('count(*) as qtd'))->get();


                $value['qtdIndicados'] = $qtd[0]->qtd;


                $val = (object) $value;
                $data[] = $val;

            }

        }else{
            $data = $this->getAllIndicadores();
        }

        return $data;
    }

    public function getAllIndicadores($id = null){

        $this->atualizaDadosIndicadores();

        if($id == null){
            $indicados = indicador::orderBy('qtdIndicados','DESC')
                ->paginate(10);

        }else{
            $indicados = indicador::orderBy('qtdIndicados','DESC')->where('id_operador', $id)
                ->paginate(10);
        }

        return $indicados;
    }

    public function atualizaDadosIndicadores(){
        $indicados = indicador::orderBy('id','DESC')->get();

        $indicadores = array();

        foreach ($indicados as $key => $value){
            $qtd = DB::table('indicado')
                ->where('idIndicador', $value['id'])
                ->select(DB::raw('count(*) as qtd'))->get();

            $qtdAprovados = DB::table('indicado')
                ->where('idIndicador', $value['id'])
                ->where('ligacaoEfetuada', '1')
                ->select(DB::raw('count(*) as qtdAprovados'))->get();


            $value['qtdIndicados'] = $qtd[0]->qtd;
            $value['qtdAprovados'] = $qtdAprovados[0]->qtdAprovados;

            $value->save();

        }

    }

    public function getQtdMembers(){
        $qtd = indicado::orderBy('id', 'DESC')->get();

       return  sizeOf($qtd);
    }


    public function getUserData(){

        return Auth::user();

    }
}

