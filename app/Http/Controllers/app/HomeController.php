<?php

namespace App\Http\Controllers\app;

use App\indicador;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\indicado;
use App\api;

class HomeController extends Controller
{
    public function index(){
        return view('app.home');
    }
    
    public function form(){
        return view('form.form');
    }

    public function indicadores(){
        return view('app.indicadores');
    }

    public function atualizaCompras(){


        $users = indicado::orderBy('idCompra', 'DESC')->get();

        $arrUsers = (object) json_decode($users);

        foreach ($arrUsers as $key => $value){

            $idCompra = $value->idCompra;

            if($idCompra){
                $url = "http://semparar.vtexcommercestable.com.br/api/oms/pvt/orders/" . $idCompra;


                $response = httpGet($url);

                $resp = json_decode($response);


                if(isset($resp->error->message)){
                    $value->statusDescription = "Pedido não encontrado";
                }else{
                    if($resp->statusDescription == "Faturado"){
                        $value->compraRealizada = 2;
                    }

                    if($resp->statusDescription == "Cancelado"){
                        $value->compraRealizada = -1;
                    }

                    $value->statusDescription = $resp->statusDescription;

                }


                $indicado = indicado::orderBy('id', 'DESC')->where('id', $value->id);


                $buscaCep = "http://api.vtexcrm.com.br/semparar/dataentities/CL/search?_where=email=".$value->email."&_fields=document,email";

                $newResponse = httpGet($buscaCep);

                $cpfResp = json_decode($newResponse);

                if($cpfResp != null){

                    $value->cpf = $cpfResp[0]->document;

                }


                $dadosIndicado = (array) $value;

                $indicado->update($dadosIndicado);

            }
        }
    }

    public function all(){
        $data['indicados'] = indicado::orderBy('indicado.id','DESC')
            ->join('indicador', 'indicado.idIndicador', '=', 'indicador.id')
            ->select('indicador.email as indicadorMail', 'indicador.nome as firstName', 'indicado.*')
            ->paginate(10);

        return view('all.index', $data);
    }

    public function sendStatusVtex(){
        $indicados = indicado::orderBy('id')->get();

        $url = "http://api.vtexcrm.com.br/".api::$loja."/dataentities/".api::$fat."/documents";


        foreach ($indicados as $key => $value){
            if($value->statusDescription == "Faturado" && $value->emailsent == 0){

                $arr['idCompra'] = $value->idCompra;
                $arr['nomeComprador'] = $value->nome;
                $arr['StatusCompra'] = $value->statusDescription;

                $indicador = indicador::findOrFail($value->idIndicador);

                $arr['nomeIndicador'] = $indicador->nome;
                $arr['emailIndicador'] = $indicador->email;


                $obj = json_encode($arr);

                $response = httpPost($url, $obj);

                $value->emailsent = 1;

                $value->save();
            }
        }

        return "Dados atualizados";
    }
}


