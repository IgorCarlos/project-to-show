<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\api;

class apiController extends Controller {

    public function addMember(Request $request) {
        $url = "http://api.vtexcrm.com.br/".api::$loja."/dataentities/".api::$md."/documents";
            
        $arr = $request->input('member');
        $size =  sizeof($request->input('member'));
        
        for($i = 0; $i < $size; $i++){
            
            if($arr[$i]['email'] != ""){
                $obj = json_encode($arr[$i]);
                
                $response = httpPost($url, $obj);
            }
        }
    }
    
    public function getMembers($inicio, $fim){

        $url = "http://api.vtexcrm.com.br/".api::$loja."/dataentities/".api::$md."/search?_fields=_all";
        
        $response = httpGet($url, $inicio, $fim);
        
        return $response;
    }

    public function getIndicadores(){
        $url = "http://api.vtexcrm.com.br/".api::$loja."/dataentities/".api::$md."/search?_fields=_all&_sort=createdIn";

        $response = httpGet($url, 0, 100);


        $array = json_decode($response);

        $newArray = [];
        $emails = [];
        foreach ($array as $key => $value){
            if(!in_array($value->emailIndicador, $emails)){
                $emails[] = $value->emailIndicador;
                $newArray[] = $value;
            }
        }

        $qtd = array_count_values(array_column($array, 'emailIndicador'));

        $c = 0;
        foreach ($qtd as $key => $value){

            $newArray[$c]->qtd = $value;
            $c++;
        }

        return $newArray;
    }

    public function getQtdMembers(){
        $url = "http://api.vtexcrm.com.br/".api::$loja."/dataentities/".api::$md."/search?_fields=_all";

        $response = httpGet($url);

        return sizeof(json_decode($response));
    }

    public function getMember($email){

        $url = "http://api.vtexcrm.com.br/".api::$loja."/dataentities/".api::$md."/search?_fields=_all&_where=email=".$email;

        $response = httpGet($url);

        return $response;
    }

    public function addCalledMember(Request $req){
        $url = "http://api.vtexcrm.com.br/".api::$loja."/dataentities/".api::$md."/documents";
        $data = json_encode($req->input());


        $response = httpPatch($url, $data);

        return $response;
    }

    public function addbuyMember(Request $req){
        $url = "http://api.vtexcrm.com.br/semparar/dataentities/".api::$md."/documents";
        $data = json_encode($req->input());


        $response = httpPatch($url, $data);

        return $response;
    }

    public function filterMembers(Request $req){
        $search = $req->input()['search'];


        $url = "http://api.vtexcrm.com.br/".api::$loja."/dataentities/".api::$md."/search?_fields=_all&_keyword=".$search;

        $response = httpGet($url);

        return $response;
    }

    public function filterBy($filter){
        switch ($filter){
            case 'ligacaoASC':
                $filter = "ligacaoEfetuada%20asc";
                break;
            case 'ligacaoDESC':
                $filter = "ligacaoEfetuada%20desc";
                break;
            case 'compraefetuadaASC':
                $filter = "compraRealizada%20asc";
                break;
            case 'compraefetuadaDESC':
                $filter = "compraRealizada%20desc";
                break;
            default:
                $filter = "";
                break;
        }


        $url = "http://api.vtexcrm.com.br/".api::$loja."/dataentities/".api::$md."/search?_fields=_all&_sort=".$filter;


        $response = httpGet($url);

        return $response;
    }
}
