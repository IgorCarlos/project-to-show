<?php

namespace App\Http\Controllers\authVtex;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class login extends Controller
{
    public function index(){
        $data['bodyClass'] = "login";
        return view('app.login', $data);
    }
}
