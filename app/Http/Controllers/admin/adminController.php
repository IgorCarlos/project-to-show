<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\indicador;

class adminController extends Controller
{
    public function addUser(Request $req){

        $this->validate($req,[
            'name' => 'required',
            'email' => 'required|confirmed|unique:users,email',
            'password' => 'required|confirmed|min:6'
        ],[
            'required' => 'O campo :attribute é obrigatório.',
            'unique' => 'Este :attribute já está cadastrado.',
            'confirmed' => 'E campo :attribute não é igual á confirmação.',
            'min' => 'A :attribute deve conter ao menos :min caracteres.'
        ],[
            'name' => 'nome',
            'email' => 'e-mail',
            'password' => 'senha'
        ]);


        try{

            $input = $req->input();

            if($input['tipo'] == 1){
                $input['tipo_descricao'] = 'Administrador';
            }else{
                $input['tipo_descricao'] = 'Operador';
            }

            $input['password'] = Hash::make($input['password']);

            User::create($input);


        }catch(\PDOException $e){
            echo $e;
        }
    }

    public function listUsers(){
        $users = User::orderBy('id', 'desc')->paginate(10);

        return $users;
    }
     public function changeStatus($id){
        $user = User::findOrFail($id);

        $status = $user->status;
        if($status == 1){
            $user->status = 0;
        }else{
            $user->status = 1;
        }

        $user->save();

     }

     public function getUser($id){
         $user = User::findOrFail($id);

         return $user;
     }

     public function editUser($id, Request $req){

         $user = User::findOrFail($id);

         $input = (object) $req->input();

         $this->validate($req,[
             'name' => 'required',
         ],[
             'required' => 'O campo :attribute é obrigatório.',
         ],[
             'name' => 'nome',
         ]);

         if($user->email != $input->email){

             $this->validate($req,[
                 'email' => 'required|unique:users,email'
             ],[
                 'required' => 'O campo :attribute é obrigatório.',
                 'unique' => 'Este :attribute já está cadastrado.',
             ],[
                 'email' => 'e-mail',
             ]);

             $user->email = $input->email;

         }

         if($input->password != ""){
             $this->validate($req,[
                 'password' => 'required|confirmed|min:6'
             ],[
                 'required' => 'O campo :attribute é obrigatório.',
                 'unique' => 'Este :attribute já está cadastrado.',
                 'confirmed' => 'E campo :attribute não é igual á confirmação.',
                 'min' => 'A :attribute deve conter ao menos :min caracteres.'
             ],[
                 'password' => 'senha'
             ]);

             $input->password = Hash::make($input->password);
             $user->password = $input->password;
         }

         $user->name = $input->name;
         $user->tipo = $input->tipo;

         if($input->tipo == 1){
             $user->tipo_descricao = 'Administrador';
         }else{
             $user->tipo_descricao = 'Operador';
         }

         $user->save();
     }

     public function saveLog(Request $req){

         $input = $req->input();


        $user = indicador::create($input);
     }

     public function listUsersRelatorios(){
         $users = User::orderBy('id', 'desc')->paginate(10);


         foreach ($users as $key => $value){

            $qtdIndicador = indicador::orderBy('id', 'DESC')->where('id_operador', $value['id'])->count();

            $value['qtdCadastrados'] = $qtdIndicador;
         }

         return $users;
     }
}
