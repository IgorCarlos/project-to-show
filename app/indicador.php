<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class indicador extends Model
{
    public $table = "indicador";
    protected $fillable = ['nome', 'email', 'id_operador'];
}
