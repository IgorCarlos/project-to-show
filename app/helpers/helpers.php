<?php


/*$url = "http://api.vtexcrm.com.br/semparar/dataentities/VF/documents";
        $ch = curl_init();
        
        
        $data['email'] = "igor.silva@corebiz.com.br";
        $data['msg'] = "some message";
        $data['nome'] = "Igor Carlos";
        $data['nomeEmpresa'] = "Nome da empresa";
        $data['telefone'] = "11 41684502";
        $data['titulo'] = "Titulo";
        
        
        $data = json_encode($data);
        
        
        
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "accept: application/vnd.vtex.ds.v10+json",
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 172cbe5d-2d25-e1a0-b9a3-e695418cb061",
                "rest-range: resources=0-10",
                "x-vtex-api-appkey: vtexappkey-semparar-DFLTLI",
                "x-vtex-api-apptoken: SZIJAUJBOSNLHYOYCZQORZIIXJEPQYCKVTPRMXCWVSXTLHBYWOAUYMCTRZMQQTLQQDPOJWLFWUTYBPXPHFTCXHLJRVANLMFBHHDHSNOZCUYETDSBBWILAJSIRFWNVYUK"
            ),
        ));

        $result = curl_exec($ch);

        curl_close($ch);
        
        print_r($result);*/
        


        

function httpPost($url, $data){
    
        $ch = curl_init();
        
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                "accept: application/vnd.vtex.ds.v10+json",
                "cache-control: no-cache",
                "content-type: application/json",
                "rest-range: resources=0-1000",
                "x-vtex-api-appkey: vtexappkey-semparar-DFLTLI",
                "x-vtex-api-apptoken: SZIJAUJBOSNLHYOYCZQORZIIXJEPQYCKVTPRMXCWVSXTLHBYWOAUYMCTRZMQQTLQQDPOJWLFWUTYBPXPHFTCXHLJRVANLMFBHHDHSNOZCUYETDSBBWILAJSIRFWNVYUK"
            ),
        ));
        
        
        $result = curl_exec($ch);
        
        curl_close($ch);
        
        return $result;
}

function httpGet($url, $inicio = 0, $fim = 5000){
    $ch = curl_init();
        
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: application/vnd.vtex.ds.v10+json",
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 172cbe5d-2d25-e1a0-b9a3-e695418cb061",
                "rest-range: resources=".$inicio."-".$fim,
                "x-vtex-api-appkey: vtexappkey-semparar-DFLTLI",
                "x-vtex-api-apptoken: SZIJAUJBOSNLHYOYCZQORZIIXJEPQYCKVTPRMXCWVSXTLHBYWOAUYMCTRZMQQTLQQDPOJWLFWUTYBPXPHFTCXHLJRVANLMFBHHDHSNOZCUYETDSBBWILAJSIRFWNVYUK"
            ),
        ));
        
        
        $result = curl_exec($ch);
        
        curl_close($ch);
        
        return $result;
}

function httpPatch($url, $data){

    $ch = curl_init();

    curl_setopt_array($ch, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "PATCH",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            "accept: application/vnd.vtex.ds.v10+json",
            "cache-control: no-cache",
            "content-type: application/json",
            "rest-range: resources=0-1000",
            "x-vtex-api-appkey: vtexappkey-semparar-DFLTLI",
            "x-vtex-api-apptoken: SZIJAUJBOSNLHYOYCZQORZIIXJEPQYCKVTPRMXCWVSXTLHBYWOAUYMCTRZMQQTLQQDPOJWLFWUTYBPXPHFTCXHLJRVANLMFBHHDHSNOZCUYETDSBBWILAJSIRFWNVYUK"
        ),
    ));


    $result = curl_exec($ch);

    curl_close($ch);

    return $result;
}