<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class indicado extends Model
{
    public $table = 'indicado';

    protected $fillable = ['compraRealizada','email','idIndicador','ligacaoEfetuada','nome','telefone'];
}
