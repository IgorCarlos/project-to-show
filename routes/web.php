<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'app', 'middleware' => 'auth'], function(){
    
    Route::get('/', [
        'uses' =>  'HomeController@index', 'as' => 'inicio'
    ]);
    //->middleware('authVtex')
    
    Route::get('/form', [
        'uses' => 'HomeController@form', 'as' => 'form'
    ]);

    Route::get('/usuarios',[
        'uses' => 'UsuariosController@index', 'as' => 'usuarios'
    ]);

    Route::get('/indicadores', [
        'uses' => 'HomeController@indicadores', 'as' => 'indicadores'
    ]);



    Route::get('/teste', [

        'uses' => 'HomeController@all', 'as' => 'all'

    ]);


    Route::get('/userData',[
        'uses' => 'appController@getUserData'
    ]);
});


Route::get('/atualizaStatusCompra', [
    'uses' => 'app\HomeController@atualizaCompras', 'as' => 'atualizaCompra'
]);

Route::get('/enviaStatusVtex', [
    'uses' => 'app\HomeController@sendStatusVtex', 'as' => 'sendStatusVtex'
]);

Route::group(['namespace' => 'authVtex'], function(){
    Route::get('/login',[
        'uses' => 'login@index', 'as' => 'login'
    ]);
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
