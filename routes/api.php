<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['namespace' => 'app'], function(){

    Route::post('/addIndicados/{email}', [
        'uses' => 'appController@addIndicados', 'as' => 'addMembro'
    ])->middleware('cors');

    Route::get('/getId/{email}', [
        'uses' => 'appController@returnId', 'as' => 'getid'
    ])->middleware('cors');

    Route::post('/addBuyToMember', [
        'uses' => 'appController@addBuyToMember', 'as' => 'addbuyto'
    ])->middleware('cors');


    Route::get('/getAllMembers/{id?}', [
        'uses' => 'appController@getAllMembers', 'as' => 'addbuyto'
    ])->middleware('cors');

    Route::get('/getAllIndicadores/{id?}', [
        'uses' => 'appController@getAllIndicadores', 'as' => 'addbuyto'
    ])->middleware('cors');

    Route::get('/getMembers/{inicio}/{fim}', [
        'uses' => 'appController@getAllMembers', 'as' => 'addbuyto'
    ])->middleware('cors');

    Route::get('/userData', [

        'uses' => 'appController@getUserData'

    ]);


    Route::get('/filterMemberBy/{filter}', [
        'uses' => 'appController@filterMemberBy', 'as' => 'addbuyto'
    ])->middleware('cors');

    Route::match(['post','options'],'/addCalledMember', [
        'uses' => 'appController@addCalledMember'
    ])->middleware('cors');

    Route::match(['post','options'],'/addBuyMember/{id}', [
        'uses' => 'appController@addbuyMember'
    ])->middleware('cors');


    Route::get('/getQtdIndicadores', [
        'uses' => 'appController@getQtdIndicadores'
    ])->middleware('cors');

    Route::get('/getQtdMembers', [
        'uses' => 'appController@getQtdMembers'
    ])->middleware('cors');

    Route::match(['post','get'], '/filterMembers', [
        'uses' => 'appController@filterMembers'
    ])->middleware('cors');

    Route::match(['post','get'], '/filterIndicadores', [
        'uses' => 'appController@filterIndicadores'
    ])->middleware('cors');


    Route::get('/getPager', [
        'uses' => 'appController@getPagination'
    ]);


    Route::match(['post', 'get'], '/testeCors', [
        'uses' => 'appController@testecors'

    ])->middleware('cors');


});


Route::group(['namespace' => 'admin', 'prefix' => 'admin'], function(){

    Route::post('/addUser', [
        'uses' => 'adminController@addUser'
    ]);


    Route::get('/listUsers', [
        'uses' => 'adminController@listUsers'
    ]);

    Route::get('/listUsersRelatorios', [
        'uses' => 'adminController@listUsersRelatorios'
    ]);

    Route::get('/getUserData/{id}', [
        'uses' => 'adminController@getUser'
    ]);

    Route::post('/changeStatus/{id}', [
        'uses' => 'adminController@changeStatus'
    ]);

    Route::post('/editUser/{id}', [

        'uses' => 'adminController@editUser'

    ]);


    Route::post('/gravaLog',[
        'uses' => 'adminController@saveLog'
    ]);
});
