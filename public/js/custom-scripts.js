$(document).ready(function(){

    $('#cpf').mask('000.000.000-00', {reverse: true});
    $('#telefone').mask('(00) 0000-0000');
    $('#phone').mask('(00) 0000-00000');

    $('body').on('click', '.bt-add-cliente', function(){
        $('.modal-overlay').fadeToggle();
        $('.modal-overlay-over').fadeToggle();

    })


    $('.modal-cad-header button').click(function(){
        $('.modal-overlay').hide();
        $('.modal-overlay-over').hide();
    });


    $('body').on('submit','.cad-cliente', function(){
        var form = $(this);

        var validate = null;
        form.find('input').each(function(){
            if($(this).val() == ""){
                $(this).css('border','1px solid red');

                validate = true;

            }else{
                $(this).css('border','1px solid #d7d7d7');
            }
        });


        if(validate == null){

            var data = {
                'firstName' : $(this).find('#nome').val(),
                'lastName' : $(this).find('#sobrenome').val(),
                'email' : $(this).find('#email').val(),
                'document' : $(this).find('#cpf').val().replace(/[^\d]+/g,''),
                'documentType' : 'cpf',
                'homePhone' : $(this).find('#telefone').val().replace(/[^\d]+/g,''),
                'phone' :  $(this).find('#phone').val().replace(/[^\d]+/g,'')
            };

            var dataBanco = {
                'nome' : $(this).find('#nome').val(),
                'email' : $(this).find('#email').val(),
                'id_operador' : window.Laravel.userData.id
            }

            var emailIndicador = $(this).find('#email').val();

            var url = "//api.vtexcrm.com.br/dataentities/CL/documents";

            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: 'POST',
                headers: {
                    'accept' : 'application/json',
                    'Content-Type' : 'application/json',
                    'x-vtex-api-appkey' : 'vtexappkey-semparar-DFLTLI',
                    'x-vtex-api-apptoken' : 'SZIJAUJBOSNLHYOYCZQORZIIXJEPQYCKVTPRMXCWVSXTLHBYWOAUYMCTRZMQQTLQQDPOJWLFWUTYBPXPHFTCXHLJRVANLMFBHHDHSNOZCUYETDSBBWILAJSIRFWNVYUK'
                },
                success: function (res) {
                    form.find('input').each(function(){
                        $(this).attr('disabled', 'disabled');
                    });

                    form.find('button').attr('disabled','disabled');

                    sendLogDataBase(dataBanco);

                    $('.msgExiste').remove();
                    $('.erroValidate').remove();
                    $('.msgSucesso').remove();
                    $('.cad-cliente').append("<p class='msgSucesso'>Usuário cadastrado com sucesso, para indicar amigos <a target='_blank' href='http://semparar.vtexcommercestable.com.br/indique-amigos?indicador="+emailIndicador+"'>Clique aqui</a></p>")
                },
                error: function (res) {
                    $('.msgExiste').remove();
                    $('.erroValidate').remove();
                    $('.msgSucesso').remove();
                    var erro = JSON.parse(res.responseText);
                    
                    if(erro.Message == "O documento já existe"){
                        $('.cad-cliente').append("<p class='msgExiste'>Este usuário já esta cadastrado, para indicar amigos <a target='_blank' href='http://semparar.vtexcommercestable.com.br/indique-amigos?indicador="+emailIndicador+"'>Clique aqui</a></p>")
                    }
                }
            });
        }else{

            $('.msgExiste').remove();
            $('.erroValidate').remove();
            $('.msgSucesso').remove();

            $('.cad-cliente').append("<p class='erroValidate'>Todos os campos são de preenchimento obrigatorio.</p>")
        }

        return false;
    });



    $('body').on('mouseover', '.a-icon', function(){
        $(this).addClass('tooltip');
    });

    $('body').on('mouseleave', '.a-icon', function(){
        $(this).removeClass('tooltip');
    });

    $('body').on('click','.a-icon.compra', function(){

        $('.modal-overlay-over.modalCompra').fadeToggle();
        $('.modal-overlay.modalCompra').fadeToggle();
        $('#idCliente').val($(this).attr('data-target'));
        return false;

    });

    $('body').on('submit', '.cad-compra', function(){

        var url = 'api/addBuyMember/' + $('input#idCliente').val();

        var id = $(this).find('#numero').val();

        console.log(id);
        if(id == ""){

            $(this).find('#numero').css('border', '1px solid red');

        }else{
            $.ajax({
                url: url,
                data: $(this).serialize(),
                type: 'POST',
                success: function(res){
                    window.location.reload();
                },
                error: function(res){
                    $('body').append(res.responseText);
                    console.log(res);
                }
            });
        }

        return false;
    })
});
function sendLogDataBase(data){

    $.ajax({

        url: 'api/admin/gravaLog',
        data: data,
        type: 'POST',
        success: function (res) {
            console.log(res);
        },
        error: function (err) {
            console.error(err);
            $('body').append(err.responseText);
        }
    });

}