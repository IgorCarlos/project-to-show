<!DOCTYPE html>
<html lang="pt-br">
<head>
    <base href="{{ asset('/') }}">
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Painel Administrativo</title>
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}"/>
</head>
<script>
    window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'userData' => Auth::user()
        ]) !!};


</script>
<?php $class = (isset($bodyClass)) ? $bodyClass : '' ?>
<body class="{{ $class }}">

@yield('container')

</body>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
    <script src="{{ asset('/js/app.js') }}"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js"></script>
    <script src="{{ asset('/js/custom-scripts.js') }}"></script>
</html>