@extends('layout.layout')
   
   
@section('container')
<div id="app">
    <form id="form-member">
       <div class="box-form-getmember">
           <span>Amigo 1</span>
           <label class="lbl-getmember">
               <input type="text" class="name" name="member[0][nome]" placeholder="Nome">
           </label>
           <label class="lbl-getmember">
               <input type="text" class="telefone" name="member[0][email]" placeholder="E-mail">
           </label>
           <label class="lbl-getmember">
               <input type="text" class="email" name="member[0][telefone]" placeholder="Telefone">
           </label>
       </div>

       <div class="box-form-getmember">
           <span>Amigo 2</span>
           <label class="lbl-getmember">
               <input type="text" class="name" name="member[1][nome]" placeholder="Nome">
           </label>
           <label class="lbl-getmember">
               <input type="text" class="telefone" name="member[1][email]" placeholder="E-mail">
           </label>
           <label class="lbl-getmember">
               <input type="text" class="email" name="member[1][telefone]" placeholder="Telefone">
           </label>
       </div>
       
       <div class="box-form-getmember">
           <span>Amigo 3</span>
           <label class="lbl-getmember">
               <input type="text" class="name" name="member[2][nome]" placeholder="Nome">
           </label>
           <label class="lbl-getmember">
               <input type="text" class="telefone" name="member[2][email]" placeholder="E-mail">
           </label>
           <label class="lbl-getmember">
               <input type="text" class="email" name="member[2][telefone]" placeholder="Telefone">
           </label>
       </div>
       
       <div class="box-form-getmember">
           <span>Amigo 4</span>
           <label class="lbl-getmember">
               <input type="text" class="name" name="member[3][nome]" placeholder="Nome">
           </label>
           <label class="lbl-getmember">
               <input type="text" class="telefone" name="member[3][email]" placeholder="E-mail">
           </label>
           <label class="lbl-getmember">
               <input type="text" class="email" name="member[3][telefone]" placeholder="Telefone">
           </label>
       </div>
       
       <div class="box-form-getmember">
           <span>Amigo 5</span>
           <label class="lbl-getmember">
               <input type="text" class="name" name="member[4][nome]" placeholder="Nome">
           </label>
           <label class="lbl-getmember">
               <input type="text" class="telefone" name="member[4][email]" placeholder="E-mail">
           </label>
           <label class="lbl-getmember">
               <input type="text" class="email" name="member[4][telefone]" placeholder="Telefone">
           </label>
       </div>


       <div class="button-send">
          <input type="submit" value="Enviar"/>
       </div>
    </form>
    <div class="debug"></div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script>
    $(document).ready(function(){
        $('#form-member').submit(function(e){
           
            e.preventDefault();
            
            $.ajax({
                url: '/api/addMember',
                type: "POST",
                data: $(this).serialize(),
                success: function(res){
                    console.log(res);
                },
                error: function(res){
                    console.log(res);
                    $('.debug').html(res.responseText);
                }
            })
            
        });
    });
</script>
@endsection