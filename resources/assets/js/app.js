
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.use(VueResource);
Vue.use(VueRouter);

/*Vue.component('indicadores', require('./components/app/indicadores.vue'));

Vue.component('menutelevendas', require('./components/app/menuTelevendas.vue'));

Vue.component('login', require('./components/app/login.vue'));

Vue.component('application', require('./components/app/app.vue'));

Vue.component('cabecalho', require('./components/app/header.vue'));

Vue.component('navigator', require('./components/app/menu.vue'));

Vue.component('usuarios', require('./components/app/usuarios.vue'));

Vue.component('form-user', require('./components/app/form-cadastro.vue'));

Vue.component('form-compra', require('./components/app/adicionarCompra.vue'));
*/

import App from './components/App.vue';

import { routes } from './components/router';

const router = new VueRouter({
    routes,
    mode: 'hash'
});

const app = new Vue({
    el: '#app',
    router,
    render: h => h(App)
});

