import application from './app/app.vue';
import indicadores from './app/indicadores.vue';
import usuarios from './app/usuarios.vue';
import addusuario from './app/addusuario.vue';
import editUsuario from './app/editUsuario.vue'
import relatorios from './app/relatorios.vue';

function dynamicPropsFn (route) {
    return {
        idIndicador: route.params.id
    }
}

function propIdEditar (route) {
    return{
        editId: route.params.id
    }
}

function propId (route) {
    return{
        idBusca: route.params.id
    }
}


export const routes = [
    {path: '/', component: application },
    {path: '/indicadores', component: indicadores },
    {path: '/indicados/:id', component: application, props: dynamicPropsFn },
    {path: '/usuarios',name: 'users', component: usuarios},
    {path: '/usuarios/novo', name: 'addUsuario', component: addusuario},
    {path: '/usuarios/editar/:id', name: 'editarUsuario', component: editUsuario, props: propIdEditar},
    {path: '/usuarios/relatorios', name: 'relatorios', component: relatorios},
    {path: '/relatorios/:id', name: 'relatorio_detalhe', component: indicadores, props: propId}
]
